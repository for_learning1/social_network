'use strict'

// var User = require('../models/user');
var Follow = require('../models/follow');
require('mongoose-pagination');

function saveFollow(req, res) {
    var params = req.body;

    var follow = new Follow();
    follow.user = req.user.sub;
    follow.followed = params.followed;

    follow.save((err, followStored) => {
        if (err) {
            console.error(err);
            return res.status(500).send({ message: 'Error saving follow' });
        }
        if (!followStored) return res.status(404).send({ message: 'Follow has not been saved' });
        return res.status(200).send({ follow: followStored });
    });
}

function deleteFollow(req, res) {
    var userId = req.user.sub;
    var followId = req.params.id;

    Follow.find({
        user: userId,
        followed: followId
    })
    .remove(err => {
        if (err) {
            console.error(err);
            return res.status(500).send({ message: 'Error deleting follow' });
        }
        return res.status(200).send({ message: 'Follow deleted' });
    });
}

function getFollowingUsers(req, res) {
    var userId = req.user.sub;
    if (req.params.id) {
        userId = req.params.id;
    }
    var page = 1;
    if (req.params.page) {
        page = req.params.page;
    }
    var itemsPerPage = 4;

    Follow.find({ user: userId }).populate({ path: 'followed' })
        .paginate(page, itemsPerPage, (err, follows, total) => {
            if (err) {
                console.error(err);
                return res.status(500).send({ message: 'Error fetching following' });
            }

            if (!follows) return res.status(404).send({ message: 'Not following any users' });

            return res.status(200).send({ follows, total, pages: Math.ceil(total/itemsPerPage) });
        });
}

function getFollowers(req, res) {
    var userId = req.user.sub;
    if (req.params.id) {
        userId = req.params.id;
    }
    var page = 1;
    if (req.params.page) {
        page = req.params.page;
    }
    var itemsPerPage = 4;

    Follow.find({ followed: userId }).populate({ path: 'user followed' })
        .paginate(page, itemsPerPage, (err, followers, total) => {
            if (err) {
                console.error(err);
                return res.status(500).send({ message: 'Error fetching followers' });
            }

            if (!followers) return res.status(404).send({ message: 'Not followers' });

            return res.status(200).send({ followers, total, pages: Math.ceil(total/itemsPerPage) });
        });
}

function getMyFollows(req,res) {
    var userId = req.user.sub;

    var find = Follow.find({ user: userId });
    if (req.params.followed) {
        find = Follow.find({ followed: userId });
    }

    find.populate({ path: 'user followed' })
        .exec((err, follows) => {
            if (err) {
                console.error(err);
                return res.status(500).send({ message: 'Error fetching getMyFollows' });
            }

            if (!follows) return res.status(404).send({ message: 'Not getMyFollows' });

            return res.status(200).send({ follows });
        });
}

module.exports = {
    saveFollow,
    deleteFollow,
    getFollowingUsers,
    getFollowers,
    getMyFollows
}
