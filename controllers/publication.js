'use strict'

const path = require('path')
const fs = require('fs')
const moment = require('moment')
require('mongoose-pagination')

const Follow = require('../models/follow')
const Publication = require('../models/publication')
const User = require('../models/user')

function savePublication(req, res) {
    const params = req.body
    
    if (!params.text) res.status(322).send({ message: 'Text field is required' })
    
    const publication = new Publication()
    publication.text = params.text
    publication.file = null
    publication.user = req.user.sub
    publication.created_at = moment().unix()

    publication.save((err, publicationStored) => {
        if (err) {
            console.error(err)
            return res.status(500).send({ message: 'Error saving publication' })
        }
        if (!publicationStored) return res.status(404).send({ message: 'Publication has not been saved' })
        return res.status(200).send({ publication: publicationStored })
    })
}

const getPublications = async (req, res) => {
    let page = 1
    if (req.params.page) {
        page = req.params.page
    }
    const itemsPerPage = 4

    const follows = await Follow.find({ user: req.user.sub }).populate('followed')
    const follows_clean = follows.map(follow => follow.followed)

    const getPublicationPaginate = () => {
        return new Promise((resolve, reject) => {
            Publication.find({ user: { '$in': follows_clean } })
                .sort('-created_at')
                .populate('user')
                .paginate(page, itemsPerPage, (err, publications, total) => {
                    resolve({ publications, total })
                })
        })
    }

    const { publications, total } = await getPublicationPaginate()

    return res.status(200).send({ total, page, pages: Math.ceil(total/itemsPerPage), publications })

}

const getPublication = async (req, res) => {
    const publicationId = req.params.id
    try {
        const publication = await Publication.findById(publicationId)
        if (!publication) return res.status(404).send({ message: 'No publication' })
        return res.status(200).send({ publication })
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: 'Error fetching publication' })
    }
}

const deletePublication = async (req, res) => {
    const publicationId = req.params.id
    try {
        const publicationRemoved = await Publication.find({ user: req.user.sub, _id: publicationId}).remove()
        if (!publicationRemoved) return res.status(404).send({ message: 'Publication could not be removed' })
        console.log(publicationRemoved)
        return res.status(200).send({ message: 'Publication removed' })
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: 'Error deleting publication' })
    }
}

module.exports = {
    savePublication,
    getPublications,
    getPublication,
    deletePublication
}
