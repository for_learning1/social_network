'use strict'

var bcrypt = require('bcrypt-nodejs');
var fs = require('fs');
var path = require('path');
require('mongoose-pagination');

var jwt = require('../services/jwt')
var User = require('../models/user');
var Follow = require('../models/follow');

function home(req, res) {
    res.status(200).send({
        message: 'Hello world from social network'
    })
}

function saveUser(req, res) {
    var params = req.body;
    var user = new User();

    if (
        params.name &&
        params.surname &&
        params.nick &&
        params.email &&
        params.password
        ) {
            user.name = params.name;
            user.surname = params.surname;
            user.nick = params.nick;
            user.email = params.email;
            user.role = 'ROLE_USER';
            user.image = null;

            User.find({
                $or: [
                    { email: user.email.toLowerCase() },
                    { nick: user.nick.toLowerCase() }
                ]
            }).exec((err, users) => {
                if (err) {
                    console.error(err);
                    return res.status(500).send({ message: 'Error checking user' });
                }

                if (users && users.length >= 1) {
                    return res.status(422).send({ message: 'User already registered' });
                } else {
                    bcrypt.hash(params.password, null, null, (err, hash) => {
                        user.password = hash;
        
                        user.save((err, userStored) => {
                            if (err) {
                                console.error(err);
                                return res.status(500).send({ message: 'Error saving user' });
                            }
        
                            if (userStored) {
                                res.status(200).send({ user: userStored });
                            } else {
                                res.status(404).send({ message: 'User has not been saved' });
                            }
        
                        });
        
                    });
                }
            })

    } else {
        res.status(422).send({
            message: 'Missing fields'
        });
    }
    
}

function loginUser(req, res) {
    var params = req.body;

    var email = params.email;
    var password = params.password;

    User.findOne({ email }, (err, user) => {
        if (err) {
            console.error(err);
            return res.status(500).send({ message: 'Error saving user' });
        }

        if (user) {
            bcrypt.compare(password, user.password, (err, check) => {
                if (check) {
                    if (params.gettoken) {
                        return res.status(200).send({ token: jwt.createToken(user) });
                        
                    } else {
                        user.password = undefined;
                        return res.status(200).send({ user });
                    }
                } else {
                    return res.status(404).send({ message: 'Wrong credentials' });
                }
            });
        } else {
            return res.status(404).send({ message: 'Wrong credentials' });
        }
    })
}

const getUser = async (req, res) => {
// async function getUser(req, res) {
    const userId = req.params.id

    let user = await User.findById(userId).select({ _id:0, password: 0, __v:0 })
    let follow = await Follow.findOne({ user: req.user.sub, followed: userId })

    return res.status(200).send({ user, follow })

    // User.findById(userId, (err, user) => {
    //     if (err) return res.status(500).send({ message: 'Error fetching user' });
    //     if (!user) return res.status(404).send({ message: 'User not found' });
    //     Follow.findOne({ user: req.user.sub, followed: userId })
    //         .exec((err, follow) => {
    //             if (err) return res.status(500).send({ message: 'Error fetching user' });
    //             return res.status(200).send({ user, follow });
    //         });
    // })
}

function getUsers(req, res) {
    var page = 1;

    if (req.params.page) {
        page = req.params.page;
    }

    var itemsPerPage = 5;

    User.find().sort('_id').paginate(page, itemsPerPage, (err, users, total) => {
        if (err) return res.status(500).send({ message: 'Error fetching users' });
        if (!users) return res.status(404).send({ message: 'Users not found' });
        return res.status(200).send({ users, total, pages: Math.ceil(total/itemsPerPage) });
    });
}

function updateUser(req, res) {
    var userId = req.params.id;
    var update = req.body;

    delete update.password;

    if (userId != req.user.sub) {
        return res.status(403).send({ message: 'You cannot change user data' });
    }

    User.findByIdAndUpdate(userId, update, { new: true }, (err, userUpdated) => {
        if (err) return res.status(500).send({ message: 'Error updating user' });
        if (!userUpdated) return res.status(404).send({ message: 'User not found' });
        return res.status(200).send({ user: userUpdated });
    })
}

function uploadImage(req, res) {
    var userId = req.params.id;
    

    if (req.files && req.files.image) {
        var file_path = req.files.image.path;

        if (userId != req.user.sub) {
            return deleteFile(res, file_path, 'You cannot change user data');
        }
        
        var file_split = file_path.split('/');
        var file_name = file_split[2];
        var ext_split = file_name.split('.');
        var file_ext = ext_split[1];

        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg' || file_ext == 'gif' ) {
            User.findByIdAndUpdate(userId, { image: file_name }, { new: true }, (err, userUpdated) => {
                if (err) return res.status(500).send({ message: 'Error updating user' });
                if (!userUpdated) return res.status(404).send({ message: 'User not found' });
                return res.status(200).send({ user: userUpdated });
            });
        } else {
            return deleteFile(res, file_path, 'Image extension not valid');
        }
    } else {
        return res.status(422).send({ message: 'files are required' });
    }
}

function deleteFile(res, file_path, message) {
    fs.unlink(file_path, (err) => {
        return res.status(422).send({ message });
    })
}

function getImageFile(req, res) {
    var file_image = req.params.fileImage;
    var file_path = './uploads/users/' + file_image;

    fs.exists(file_path, exists => {
        if (exists) {
            res.sendFile(path.resolve(file_path));
        } else {
            return res.status(404).send({ message: 'Image not found' });
        }
    })
}

const getCounters = async (req, res) => {
    const userId = req.params.id
    const following = await Follow.count({ user: userId })
    const followers = await Follow.count({ followed: userId })
    return res.status(200).send({ following, followers })
}

module.exports = {
    home,
    saveUser,
    loginUser,
    getUser,
    getUsers,
    updateUser,
    uploadImage,
    getImageFile,
    getCounters
}
