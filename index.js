'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = 3000;

// database connection

mongoose.Promise = global.Promise;
// mongodb://marlon:marlon@db:27017/?authSource=admin&readPreference=primary&ssl=false
mongoose.connect('mongodb://marlon:marlon@db:27017/social_network_db?authSource=admin&readPreference=primary&ssl=false', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => {
        console.log('database connection was made successfully');

        app.listen(port, () => {
            console.log('Server running on http://localhost:9000')
        })
    })
    .catch(error => {
        console.log('there was an error trying to connect to database');
        console.error(error);
    })
