'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'este_es_el_super_secret';

exports.ensureAuth = function(req, res, next) {
    if (!req.headers.authorization) return res.status(403).send({ message: 'Request does not have Authorization header' });

    var token = req.headers.authorization.replace(/['"]+/g, '');

    try {
        var payload =jwt.decode(token, secret);
        if (payload.exp <= moment().unix()) {
            return res.status(401).send({ message: 'Token expired' });
        }
    } catch (error) {
        return res.status(404).send({ message: 'Token not valid' });
    }
    req.user = payload;
    next();
}